# Discord to Google Cloud Pub/Sub Agent

Publishes Discord events to Google Cloud Pub/Sub

## Hosting

The agent can be used as an event source for one or more "classic" Discord bot clients. When hosting the agent, make sure the bots (and sharding combinations thereof) are hosted on a single instance to avoid duplication.

On Google Cloud, the agent may either be hosted on Compute Engine or on Kubernetes Engine. Do not host the agent on App Engine as App Engine will provision more than one instance of the app.

## Options

* discordClients - array of (discord.js client or options object)
  * displayID - human-friendly alias for the client. Defaults to the token
  * token - Discord API token
  * options - discord.js options
* defaultDiscordOptions - discord.js options
* pubsubOptions - Cloud Pub/Sub options
* includeMe - Whether to include operations done by the user / bot user
* includeBotMessages - Whether to include bot actions

## Supported events

Events will be sent to `$displayID.$event`

* message (content, { id, channelId })
* messageDelete (, { id })
* messageReactionAdd, messageReactionRemove (, { emoji, me, count, messageId, channelId })
