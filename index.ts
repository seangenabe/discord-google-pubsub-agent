import { Agent } from './agent'
import cosmiconfig from 'cosmiconfig'
import exitHook from 'async-exit-hook'
import { callbackify } from 'util'
import { log } from './log'

export { Agent }

export async function run() {
  try {
    const explorer = cosmiconfig('dgpa')
    const result = await explorer.search()
    if (result == null) {
      throw new Error('No config')
    }
    const { config } = result

    const agent = new Agent(config as any)
    exitHook(
      callbackify(async () => {
        log.info('destroy called')
        await agent.destroy()
      })
    )
    log.info('connecting')
    await agent.connect()
  } catch (err) {
    console.error(err)
    log.error(err)
    process.exit(1)
  }
}

if (require.main === module) {
  run()
}
