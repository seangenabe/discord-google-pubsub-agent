FROM bitnami/node:10

WORKDIR /app

# Instal app dependencies
COPY package*.json ./

# Install dependencies
RUN npm ci -d --unsafe

# Bundle app source
COPY . .

# Build
RUN npm run tsc

# Delete source files
RUN rm *.ts

# Prune dev deps
RUN npm prune --production

CMD npm start
