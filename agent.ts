import nn from '@seangenabe/nn'
import { ok } from 'assert'
import { Client, ClientOptions, MessageReaction, User } from 'discord.js'
import PubSub from '@google-cloud/pubsub'
import { log } from './log'

export class Agent {
  public readonly discordClients: Map<string, DiscordClientInfo>
  public readonly pubsubClient: PubSub.PubSub
  private readonly includeMe: boolean
  private readonly includeBotMessages: boolean

  constructor(opts: AgentOptions) {
    const {
      discordClients,
      defaultDiscordOptions = {},
      pubsubOptions = {},
      includeMe,
      includeBotMessages
    } = opts

    this.includeMe = includeMe
    this.includeBotMessages = includeBotMessages

    this.discordClients = new Map()
    for (let o of discordClients) {
      if (o instanceof Client) {
        const token = nn(o.token)
        this.discordClients.set(token, {
          displayID: token,
          client: o
        })
        continue
      }

      ok(typeof o === 'object')
      let { token, options = {}, displayID } = o
      displayID = displayID || token
      ok(token)
      options = { ...defaultDiscordOptions, ...options }
      const client = new Client()
      this.discordClients.set(token, { displayID, client })
    }

    this.pubsubClient = PubSub(pubsubOptions)
  }

  private async publish(
    info: DiscordClientInfo,
    event: string,
    data: Buffer | string | any,
    attributes?: Record<string, any>
  ): Promise<void> {
    const topicName = `${info.displayID}.${event}`
    log.trace({ topicName })
    const topic: PubSub.Topic = await this.pubsubClient.topic(topicName)

    // Check if topic exists.
    if (!(await topic.exists())) {
      return
    }

    // Publish to topic.
    let buf: Buffer
    if (
      Buffer.isBuffer(data) ||
      data instanceof ArrayBuffer ||
      typeof data === 'string'
    ) {
      buf = Buffer.from(data)
    } else {
      buf = Buffer.from(JSON.stringify(data))
    }
    topic.publisher().publish(buf, attributes)
  }

  async connect() {
    const promises: Promise<void>[] = []
    for (let [token, info] of this.discordClients) {
      const { client } = info
      promises.push(
        (async () => {
          const pub = (
            event: string,
            data: Buffer | string | any,
            attributes?: Record<string, any>
          ) => this.publish(info, event, data, attributes)

          client.on('message', async message => {
            if (
              (this.includeBotMessages && message.author.bot) ||
              (this.includeMe && message.author.id === client.user.id)
            ) {
              return
            }
            await pub('message', message.content, {
              id: message.id,
              channelId: message.channel.id
            })
          })

          client.on('messageDelete', async message => {
            if (this.includeMe && message.author.id === client.user.id) {
              return
            }
            await pub('messageDelete', '', { id: message.id })
          })

          for (let event of ['messageReactionAdd', 'messageReactionRemove']) {
            client.on(
              event,
              async (messageReaction: MessageReaction, user: User) => {
                if (this.includeMe && user.id === client.user.id) {
                  return
                }

                let { name, id } = messageReaction.emoji
                let { me, count } = messageReaction

                await pub(event, '', {
                  emoji: { name, id },
                  me,
                  count,
                  messageId: messageReaction.message.id,
                  channelId: messageReaction.message.channel.id
                })
              }
            )
          }

          await client.login(token)
        })()
      )
    }
  }

  async destroy() {
    await Promise.all(
      [...this.discordClients.values()].map(info => info.client.destroy())
    )
  }
}

export interface AgentOptions {
  discordClients: (
    | { displayID?: string; token: string; options?: ClientOptions }
    | Client)[]
  defaultDiscordOptions?: ClientOptions
  pubsubOptions: PubSub.GCloudConfiguration
  includeMe: boolean
  includeBotMessages: boolean
}

export interface DiscordClientInfo {
  displayID: string
  client: Client
}
